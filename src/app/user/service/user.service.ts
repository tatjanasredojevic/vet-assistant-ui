import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../model/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpClient: HttpClient) { }

  private readonly baseUserUrl = "http://localhost:8080/api/v1/user";

  public signUp(user: User) : Observable<User> {
    return this.httpClient.post<User>(this.baseUserUrl, user);
  }

  //TODO: obrisati kasnije
  public getByUsername(username: string) : Observable<User> {
    return this.httpClient.get<User>(`${this.baseUserUrl}/username/${username}`);
  }

  public getAll() : Observable<User[]> {
    return this.httpClient.get<User[]>(this.baseUserUrl);
  }
}
