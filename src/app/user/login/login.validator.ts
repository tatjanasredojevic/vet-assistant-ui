import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';


export function usernameValidation(): ValidatorFn{
    return (control : AbstractControl) : ValidationErrors | null => {
        const usernameIsCorrect = 
            control.value.length > 1;
        return usernameIsCorrect ? null : {incorectUsername : true};   
            
    }
}

export function passwordValidation(): ValidatorFn{
    return (control : AbstractControl) : ValidationErrors | null => {
        const  passwordIsCorrect = 
            control.value.length > 3;
        return  passwordIsCorrect ? null : {incorectPassword : true};   
            
    }
}