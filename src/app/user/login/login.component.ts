import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth/auth.service';
import { UserService } from '../service/user.service';
import { passwordValidation, usernameValidation } from '../signup/signup.validator';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup;

  constructor(private formBilder: FormBuilder,
              private authService: AuthService,
              private router: Router,
              private snackBar: MatSnackBar) { 
    
  }

  ngOnInit(): void {
    this.loginForm = this.formBilder.group({
      username : ['',[usernameValidation()]],
      password: ['',[Validators.required, passwordValidation()]]
    });
  }
  public username(){
    return this.loginForm.get('username');
  }
  public password(){
    return this.loginForm.get('password');
  }

  public onSubmit(data) {
    console.log(data);

    this.authService.login(this.username().value, this.password().value).subscribe(user => {
      console.log(user);
      this.router.navigate(['']);
    }, (error) => {
      console.log(error)
      this.snackBar.open('Invalid username or/and password','', {
        duration: 5000
      });
      
    });
  }
}
