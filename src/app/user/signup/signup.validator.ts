import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';


export function usernameValidation(): ValidatorFn{
    return (control : AbstractControl) : ValidationErrors | null => {
        const usernameIsCorrect = 
            control.value.length > 1;
        return usernameIsCorrect ? null : {incorectUsername : true};   
            
    }
}
export function lastnameValidation(): ValidatorFn{
    return (control : AbstractControl) : ValidationErrors | null => {
        const  lastnameIsCorrect = 
            control.value.length > 1;
        return  lastnameIsCorrect ? null : {incorectLastname : true};   
            
    }
}
export function nameValidation(): ValidatorFn{
    return (control : AbstractControl) : ValidationErrors | null => {
        const  nameIsCorrect = 
            control.value.length > 2;
        return  nameIsCorrect ? null : {incorectName : true};   
            
    }
}

export function passwordValidation(): ValidatorFn{
    return (control : AbstractControl) : ValidationErrors | null => {
        const  passwordIsCorrect = 
            control.value.length > 2;
        return  passwordIsCorrect ? null : {incorectPassword : true};   
            
    }
}