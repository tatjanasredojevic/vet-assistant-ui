import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { User } from '../model/user.model';
import { UserService } from '../service/user.service';
import { usernameValidation, passwordValidation, nameValidation, lastnameValidation } from './signup.validator';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  public signupForm: FormGroup;

  constructor(private formBilder: FormBuilder,
              private userService: UserService,
              private router: Router,
              private snackBar: MatSnackBar) {
    
  }

  ngOnInit(): void {
    this.signupForm = this.formBilder.group({
      username : ['',[usernameValidation()]],
      password: ['',[Validators.required, passwordValidation()]],
      name:['',[Validators.required, nameValidation()]],
      lastname: ['',[Validators.required, lastnameValidation()]],
      email: ['',[Validators.required, Validators.email]],
      birthday: ['',[]],
      address: ['',[Validators.pattern('(.){3,}')]]
    });
  }

  public username(){
    return this.signupForm.get('username');
  }
  public password(){
    return this.signupForm.get('password');
  }
  public lastname(){
    return this.signupForm.get('lastname');
  }
  public name(){
    return this.signupForm.get('name');
  }
  public email(){
    return this.signupForm.get('email');
  }
  public birthday(){
    return this.signupForm.get('birthday');
  }
  public address(){
    return this.signupForm.get('address');
  }

  public submitForm(data){
    console.log(data);
    
    var user: User = {
      id: 0,
      name: this.name().value,
      lastname: this.lastname().value,
      address: this.address().value,
      username: this.username().value,
      password: this.password().value,
      birthday: this.birthday().value,
      email: this.email().value
    };

    console.log(user);

    this.userService.signUp(user).subscribe((user: User) => {
      console.log(user);
      this.router.navigateByUrl('/login');
      this.snackBar.open('Your registration was successful', '', {
        duration: 5000
      });
    }, (error) => {
      console.error(error);
      this.snackBar.open('Error on server side: User cannot be registered', '', {
        duration: 5000
      });
    });
  }

}
