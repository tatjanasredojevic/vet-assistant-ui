export interface User {
    id?: number,
    username: string,
    password: string,
    email: string,
    name: string,
    lastname: string,
    birthday?: Date,
    address?: string,
    phoneNumber?: string
}