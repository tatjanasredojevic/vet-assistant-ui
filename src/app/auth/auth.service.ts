import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from  'rxjs/operators';
import { User } from '../user/model/user.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private httpClient: HttpClient) { }

  private user: User;

  private readonly baseUserUrl = "http://localhost:8080/api/v1/user";

  public login(username:string, password:string) : Observable<User> {
    return this.httpClient.post<User>(this.baseUserUrl + "/login", {
      username: username,
      password: password
    }).pipe(tap(user => {
      this.user = user;
      return user;
    }));
  }

  public logout() {
    this.user = null;
  }

  getUser(): User {
    return this.user;
  }

}
