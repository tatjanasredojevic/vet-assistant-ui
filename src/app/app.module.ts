import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { SignupComponent } from './user/signup/signup.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';
import { LoginComponent } from './user/login/login.component';
import { HomeComponent } from './home/home.component';
import { NavigationComponent } from './navigation/navigation.component';
import { AppRoutingModule } from './app-routing.module';
import { AddAnimalTypeComponent } from './animal-type/add-animal-type/add-animal-type.component';
import { SearchAnimalTypeComponent } from './animal-type/search-animal-type/search-animal-type.component';
import { AddServiceTypeComponent } from './service-type/add-service-type/add-service-type.component';
import { SearchServiceTypeComponent } from './service-type/search-service-type/search-service-type.component';
import { AddOwnerComponent } from './owner/add-owner/add-owner.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatCheckboxModule}  from '@angular/material/checkbox';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatSelectModule } from '@angular/material/select';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { EditServiceTypeDialogComponent } from './service-type/edit-service-type-dialog/edit-service-type-dialog.component';
import { ConfirmDialogComponent } from './shared/confirm-dialog/confirm-dialog.component';
import { EditAnimalTypeDialogComponent } from './animal-type/edit-animal-type-dialog/edit-animal-type-dialog.component';
import { SearchOwnerComponent } from './owner/search-owner/search-owner.component';
import { EditOwnerDialogComponent } from './owner/edit-owner-dialog/edit-owner-dialog.component';
import { AddCardboarComponent } from './cardboard/add-cardboard/add-cardboard.component';
import { SearchCardboardComponent } from './cardboard/search-cardboard/search-cardboard.component';
import { EditCardboardDialogComponent } from './cardboard/edit-cardboard-dialog/edit-cardboard-dialog.component';
import { OpenCardboardComponent } from './cardboard/open-cardboard/open-cardboard.component';
import { AboutComponent } from './about/about/about.component';
import { InterventionsComponent } from './cardboard/open-cardboard/interventions/interventions.component';
import { ServicesComponent } from './cardboard/open-cardboard/interventions/services/services.component';
import { AddInterventionComponent } from './cardboard/open-cardboard/interventions/add-intervention/add-intervention.component';
import { AuthGuardService } from './auth/auth-guard.service';
import { HomePageComponent } from './home-page/home-page.component';
import { ViewOwnerDialogComponent } from './owner/view-owner-dialog/view-owner-dialog.component';



@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    LoginComponent,
    HomeComponent,
    NavigationComponent,
    AddAnimalTypeComponent,
    SearchAnimalTypeComponent,
    AddServiceTypeComponent,
    SearchServiceTypeComponent,
    AddOwnerComponent,
    EditServiceTypeDialogComponent,
    ConfirmDialogComponent,
    EditAnimalTypeDialogComponent,
    SearchOwnerComponent,
    EditOwnerDialogComponent,
    AddCardboarComponent,
    SearchCardboardComponent,
    EditCardboardDialogComponent,
    OpenCardboardComponent,
    AboutComponent,
    InterventionsComponent,
    ServicesComponent,
    AddInterventionComponent,
    HomePageComponent,
    ViewOwnerDialogComponent
  ],
  imports: [
    BrowserModule,
    NoopAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatButtonModule,
    MatDialogModule,
    MatSnackBarModule,
    MatCheckboxModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatTableModule,
    MatPaginatorModule,
    MatExpansionModule
  ],
  providers: [AuthGuardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
