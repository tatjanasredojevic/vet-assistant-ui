import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Owner } from '../model/owner.model';

@Component({
  selector: 'app-view-owner-dialog',
  templateUrl: './view-owner-dialog.component.html',
  styleUrls: ['./view-owner-dialog.component.css']
})
export class ViewOwnerDialogComponent implements OnInit {

  ownerForm: FormGroup;
  
  constructor(private formBilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public owner: Owner) { }

  ngOnInit(): void {
    this.initForm();
  }

  initForm(){
    this.ownerForm = this.formBilder.group({
      name: [this.owner.name,[]],
      lastname: [this.owner.lastname,[]],
      email: [this.owner.email,[]],
      birthday: [this.owner.birthday,[]],
      phoneNumber:[this.owner.phoneNumber,[]],
      address: [this.owner.address,[]]
    })
  }

}
