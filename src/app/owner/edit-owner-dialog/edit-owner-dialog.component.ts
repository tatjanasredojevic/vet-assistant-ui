import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { nameValidation } from 'src/app/animal-type/add-animal-type/animal-type.validator';
import { Owner } from '../model/owner.model';

@Component({
  selector: 'app-edit-owner-dialog',
  templateUrl: './edit-owner-dialog.component.html',
  styleUrls: ['./edit-owner-dialog.component.css']
})
export class EditOwnerDialogComponent implements OnInit {

  ownerForm: FormGroup;

  constructor(private formBilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public owner: Owner) { }

  ngOnInit(): void {
    this.initForm();
  }

  initForm(){
    this.ownerForm = this.formBilder.group({
      name: [this.owner.name,[nameValidation()]],
      lastname: [this.owner.lastname,[]],
      email: [this.owner.email,[]],
      birthday: [this.owner.birthday,[]],
      phoneNumber:[this.owner.phoneNumber,[]],
      address: [this.owner.address,[]]
    })
  }
  
}
