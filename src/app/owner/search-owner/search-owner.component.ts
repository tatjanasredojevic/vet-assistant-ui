import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subscription } from 'rxjs';
import { EditOwnerDialogComponent } from '../edit-owner-dialog/edit-owner-dialog.component';
import { Owner } from '../model/owner.model';
import { OwnerService } from '../service/owner.service';
import { ViewOwnerDialogComponent } from '../view-owner-dialog/view-owner-dialog.component';

@Component({
  selector: 'app-search-owner',
  templateUrl: './search-owner.component.html',
  styleUrls: ['./search-owner.component.css']
})
export class SearchOwnerComponent implements OnInit, OnDestroy {

  ownerSubscription: Subscription;
  owners: Owner[] = [];
  searchedOwners: Owner[] = [];
  searchValue: string = ''; 

  constructor(private ownerService: OwnerService,
              public dialog: MatDialog,
              private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.fetchOwners();
  }
  ngOnDestroy(): void {
    this.ownerSubscription.unsubscribe();
  }
  onSearchKeyup(event) {
    this.searchedOwners = this.owners.filter(owner => {
      return owner.name.toLowerCase().startsWith(this.searchValue) || 
      owner.lastname.toLowerCase().startsWith(this.searchValue)
    }).slice();
  }

  deleteOwner(owner: Owner) {
    this.ownerService.delete(owner.id).subscribe(owner =>{
      this.snackBar.open('Vlasnik je uspešno obrisan!','', {
        duration: 5000
      });
      this.fetchOwners();
    },(error)=> {
      window.alert('Neuspesno brisanje vlasnika');
    });
  }

  editOwner(owner: Owner) {
    console.log(owner);
    const dialogRef = this.dialog.open(EditOwnerDialogComponent, {
      data: owner,
      width: '600px'
    });

    dialogRef.afterClosed().subscribe((result: Owner) => {
      if(result) {
        result = {
          ...result,
          id: owner.id
        }
        this.ownerService.update(result).subscribe((updatedOwner: Owner) => {
          this.snackBar.open('Error on server side: username already exists','', { 
            duration: 5000
          });
          this.fetchOwners();
        });
      }
    });
  }
  fetchOwners() {
    this.ownerSubscription = this.ownerService.getAll().subscribe((owners: Owner[]) => {
      this.owners = owners;
      this.searchedOwners = owners;
    });
  }

  viewOwner(owner:Owner){
    console.log(owner);
    const dialogRef = this.dialog.open(ViewOwnerDialogComponent, {
      data: owner,
      width: '600px'
    });
  }
}
