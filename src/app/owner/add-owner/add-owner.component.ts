import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { error } from 'protractor';
import { nameValidation } from 'src/app/animal-type/add-animal-type/animal-type.validator';
import { Owner } from '../model/owner.model';
import { OwnerService } from '../service/owner.service';

@Component({
  selector: 'app-add-owner',
  templateUrl: './add-owner.component.html',
  styleUrls: ['./add-owner.component.css']
})
export class AddOwnerComponent implements OnInit {
  
  ownerForm: FormGroup;

  constructor(private formBilder: FormBuilder, 
    private ownerService: OwnerService,
    private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.initEmptyForm();
  }
  public name(){
    return this.ownerForm.get('name');
  }
  public lastname(){
    return this.ownerForm.get('lastname');
  }
  public email(){
    return this.ownerForm.get('email');
  }
  public birthday(){
    return this.ownerForm.get('birthday');
  }
  public phoneNumber(){
    return this.ownerForm.get('phoneNumber');
  }
  public address(){
    return this.ownerForm.get('address');
  }
  public addOwnerForm(data){
    console.log(data);

    var owner: Owner = {
      id: 0,
      name: this.name().value,
      lastname: this.lastname().value,
      email: this.email().value,
      birthday: this.birthday().value,
      phoneNumber: this.phoneNumber().value,
      address: this.address().value
    };

    this.ownerService.save(owner).subscribe( owner =>{
      this.snackBar.open('Owner is successfully saved','', {
        duration: 5000
      });
      this.initEmptyForm();
    }, error =>{
      console.error(error);  
      this.snackBar.open('Error on server side: Owner cannot be saved','', {
        duration: 5000
      }); 
    });
  
  }
  initEmptyForm(){
    this.ownerForm = this.formBilder.group({
      name: ['',[nameValidation]],
      lastname: ['',[]],
      birthday: ['',[]],
      email: ['',[]],
      phoneNumber: ['',[]],
      address: ['',[]]
    })
  }
}
