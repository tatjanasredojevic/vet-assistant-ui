import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Owner } from '../model/owner.model';

@Injectable({
  providedIn: 'root'
})
export class OwnerService {

  private owner: Owner;
  
  private readonly baseOwnerUrl = "http://localhost:8080/api/v1/owner";

  constructor(private httpClient: HttpClient) { }

  public save(owner: Owner) : Observable<Owner> {
    return this.httpClient.post<Owner>(this.baseOwnerUrl, owner);
  }
  public getAll(): Observable<Owner[]>{
    return this.httpClient.get<Owner[]>(this.baseOwnerUrl);
  }
  public get(id: number): Observable<Owner>{
    return this.httpClient.get<Owner>(`${this.baseOwnerUrl}/${id}`);
  }
  public delete(id: number){
    return this.httpClient.delete(`${this.baseOwnerUrl}/${id}`);
  }
  public update(owner: Owner){
    return this.httpClient.put(`${this.baseOwnerUrl}/${owner.id}`,owner);
  }
}
