import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AnimalType } from '../model/animalType.model';


@Injectable({
  providedIn: 'root'
})
export class AnimalTypeService {

  constructor(private httpClient: HttpClient) {}

    private animalType: AnimalType;

    private readonly baseAnimalTypeUrl = "http://localhost:8080/api/v1/animal-types";

    public save(animalType: AnimalType) : Observable<AnimalType> {
      return this.httpClient.post<AnimalType>(this.baseAnimalTypeUrl, animalType);
    }

    public getAll() : Observable<AnimalType[]> {
      return this.httpClient.get<AnimalType[]>(this.baseAnimalTypeUrl);
    }
    public delete(id: number){
      return this.httpClient.delete(`${this.baseAnimalTypeUrl}/${id}`);
    }
    public update(animalType: AnimalType){
      return this.httpClient.put(`${this.baseAnimalTypeUrl}/${animalType.id}`,animalType);
    }
}
