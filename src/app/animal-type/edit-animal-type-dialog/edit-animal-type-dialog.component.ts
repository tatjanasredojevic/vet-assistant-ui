import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { nameValidation } from 'src/app/user/signup/signup.validator';
import { AnimalType } from '../model/animalType.model';

@Component({
  selector: 'app-edit-animal-type-dialog',
  templateUrl: './edit-animal-type-dialog.component.html',
  styleUrls: ['./edit-animal-type-dialog.component.css']
})
export class EditAnimalTypeDialogComponent implements OnInit {

  animalTypeForm: FormGroup;
  constructor(private formBilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public animalType: AnimalType) { }

  ngOnInit(): void {
    this.initForm();
  }

  initForm(){
    this.animalTypeForm = this.formBilder.group({
      name: [this.animalType.name,[nameValidation()]],
      description: [this.animalType.description, []]
    })
  }
}
