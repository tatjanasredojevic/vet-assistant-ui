import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subscription } from 'rxjs';
import { AnimalType } from 'src/app/animal-type/model/animalType.model';
import { EditAnimalTypeDialogComponent } from '../edit-animal-type-dialog/edit-animal-type-dialog.component';
import { AnimalTypeService } from '../service/animal-type.service';

@Component({
  selector: 'app-search-animal-type',
  templateUrl: './search-animal-type.component.html',
  styleUrls: ['./search-animal-type.component.css']
})
export class SearchAnimalTypeComponent implements OnInit, OnDestroy {

  animalTypesSubscription: Subscription;
  animalTypes: AnimalType[] = [];
  searchedAnimalTypes: AnimalType[] = [];
  searchValue: string = ''; 

  constructor(private animalTypeService: AnimalTypeService,
              public dialog: MatDialog,
              private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.fetchAnimalTypes();
  }

  ngOnDestroy(): void {
    if(this.animalTypesSubscription) {
      this.animalTypesSubscription.unsubscribe();
    }
  }

  onSearchKeyup(event) {
    this.searchedAnimalTypes = this.animalTypes.filter(type => {
      return type.name.startsWith(this.searchValue) || type.description.startsWith(this.searchValue)
    }).slice();
  }

  editAnimalType(type: AnimalType) {
    const dialogRef = this.dialog.open(EditAnimalTypeDialogComponent, {
      data: type,
      width: '600px'
    });

    dialogRef.afterClosed().subscribe((result: AnimalType) => {
      if(result) {
        result = {
          ...result,
          id: type.id
        }
        this.animalTypeService.update(result).subscribe((updatedAnimalType: AnimalType) => {
          this.snackBar.open('Korisničko ime već postoji','', {
            duration: 5000
          });
          this.fetchAnimalTypes();
        });
      }
    });
  }

  deleteAnimalType(type: AnimalType) {
    this.animalTypeService.delete(type.id).subscribe(type =>{
      this.snackBar.open('Tip životinje uspešno izbrisan!','', {
        duration: 5000
      });
      this.fetchAnimalTypes();
    },(error)=> {
      window.alert('Neuspesno brisanje tipa zivotinje');
    });
    //delete[this.searchedAnimalTypes.indexOf(type)];
  }

  fetchAnimalTypes() {
    this.animalTypesSubscription = this.animalTypeService.getAll().subscribe((types: AnimalType[]) => {
      this.animalTypes = types;
      this.searchedAnimalTypes = types;
    });
  }

  

}
