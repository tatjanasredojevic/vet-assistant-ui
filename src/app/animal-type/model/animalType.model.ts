export interface AnimalType{
    id?: number,
    name: string,
    description: string,
}