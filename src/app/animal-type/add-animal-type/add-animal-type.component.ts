import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AnimalType } from '../../animal-type/model/animalType.model';

import { AnimalTypeService } from '../service/animal-type.service';
import { nameValidation } from './animal-type.validator';

@Component({
  selector: 'app-add-animal-type',
  templateUrl: './add-animal-type.component.html',
  styleUrls: ['./add-animal-type.component.css']
})
export class AddAnimalTypeComponent implements OnInit {

  animalTypeForm: FormGroup;
  constructor(private formBilder: FormBuilder,
              private animalTypeService: AnimalTypeService,
              private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.initEmptyForm();
  }

  initEmptyForm() {
    this.animalTypeForm = this.formBilder.group({
      name: ['', [nameValidation()]],
      description: ['',[]]
    })
  }

  public name(){
    return this.animalTypeForm.get('name');
  }

  public description(){
    return this.animalTypeForm.get('description');
  }

  public addAnimalTypeForm(data){
    console.log(data);
    console.log(this.animalTypeForm)

    var animalType: AnimalType = {
      id: 0,
      name: this.name().value,
      description:  this.description().value
    };

    this.animalTypeService.save(animalType).subscribe(animalType => {
      this.snackBar.open('Uspešno sačuvan tip životinje!','', {
        duration: 5000
      });
      this.initEmptyForm();
    }, error=> {
      console.error(error);
    });
  }
}
