import { AnimalType } from 'src/app/animal-type/model/animalType.model';
import { Owner } from 'src/app/owner/model/owner.model';
import { Intervention } from '../open-cardboard/interventions/model/intervention.model';

export interface Cardboard{
    id: number,
    name: string,
    birthday: Date,
    createdDate: Date,
    deactivated: boolean,
    animalType: AnimalType,
    owner: Owner,
    interventions: Intervention[]
}