import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { threadId } from 'worker_threads';
import { Cardboard } from '../model/cardboard.model';

@Injectable({
  providedIn: 'root'
})
export class CardboarService {

  private cardboard: Cardboard;

  private readonly baseCardboardUrl = "http://localhost:8080/api/v1/cardboard";

  constructor(private httpClient: HttpClient) { }

  public save(cardboard: Cardboard): Observable<Cardboard> {
    return this.httpClient.post<Cardboard>(this.baseCardboardUrl, cardboard);
  }
  public getAll(): Observable<Cardboard[]> {
    return this.httpClient.get<Cardboard[]>(this.baseCardboardUrl);
  }
  public get(id: number): Observable<Cardboard> {
    return this.httpClient.get<Cardboard>(`${this.baseCardboardUrl}/${id}`);
  }
  public update(cardboard: Cardboard){
    return this.httpClient.put(`${this.baseCardboardUrl}/${cardboard.id}`,cardboard);
  }
  
}
