import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { error } from 'protractor';
import { OwnerService } from 'src/app/owner/service/owner.service';
import { Cardboard } from '../model/cardboard.model';
import { CardboarService } from '../service/cardboar.service';
import { AddInterventionComponent } from './interventions/add-intervention/add-intervention.component';
import { Intervention } from './interventions/model/intervention.model';
import { InterventionService } from './interventions/service/intervention.service';

@Component({
  selector: 'app-open-cardboard',
  templateUrl: './open-cardboard.component.html',
  styleUrls: ['./open-cardboard.component.css']
})
export class OpenCardboardComponent implements OnInit {

  cardboardForm: FormGroup;
  cardboard: Cardboard;

  constructor(private cardboardService: CardboarService,
    private activatedRoute: ActivatedRoute,
    private ownerService: OwnerService,
    private router: Router,
    private dialog: MatDialog,
    private interventionService: InterventionService,
    private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      if(params.id) {
        this.cardboardService.get(params.id).subscribe((cardboard: Cardboard) => {
          console.log(cardboard);
          this.cardboard = cardboard;
        });
      }
    });
  }

  public addIntervention(cardboard){
    //this.router.navigate(['add-intervention'], {relativeTo: this.activatedRoute});
    const dialogRef = this.dialog.open(AddInterventionComponent, {
      data: {
        cardboard: cardboard
      },
      width: '900px'  
    });

    dialogRef.afterClosed().subscribe((result: Intervention) => {
      if(result) {
        result = {
          ...result,
        }
        result.cardboard = {
          id: this.cardboard.id
        }

        console.log(result);

        this.cardboard.interventions.push(result);
        this.cardboard = {
          ...cardboard
        }
        this.interventionService.save(result).subscribe((savedInterventions: Intervention) => {
          this.snackBar.open('Intervention is successfully saved','', {
            duration: 5000
          });
          //this.fetchOwners();
        },error => {
          this.snackBar.open('Error on server side: Intervention cannot be saved','', {
            duration: 5000
          });
        });
      }
    });
  }

  getFormatedDate(date): string {
    if(date) {
      return new Date(date).toLocaleDateString();
    }
    return "";
  }
}