import { ServiceType } from 'src/app/service-type/model/serviceType.model';
import { User } from 'src/app/user/model/user.model';

export interface Service{
    id: number,
    description: string,
    user: User,
    serviceType: ServiceType
}