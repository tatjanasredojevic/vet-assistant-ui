import { Cardboard } from '../../../model/cardboard.model';
import { Service } from './service.model';

export interface Intervention{
    id: number,
    description: string,
    createdDate: Date,
    services: Service[]
    cardboard?: InterventionCardboard
}

export interface InterventionCardboard {
    id: number
}