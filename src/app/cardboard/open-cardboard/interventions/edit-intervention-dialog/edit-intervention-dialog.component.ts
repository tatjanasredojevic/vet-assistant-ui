import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Intervention } from '../model/intervention.model';

@Component({
  selector: 'app-edit-intervention-dialog',
  templateUrl: './edit-intervention-dialog.component.html',
  styleUrls: ['./edit-intervention-dialog.component.css']
})
export class EditInterventionDialogComponent implements OnInit {

  interventionForm: FormGroup;
  constructor(private formBilder: FormBuilder,
              @Inject(MAT_DIALOG_DATA) private intervention: Intervention) { }

  ngOnInit(): void {
    this.initInterventionForm();
  }
  initInterventionForm(){
    this.interventionForm = this.formBilder.group({
      name: [this.intervention.description,[]],
      birthday: [this.intervention.createdDate,[]]
    });
  }
}
