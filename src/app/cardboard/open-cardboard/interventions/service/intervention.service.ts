import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Intervention } from '../model/intervention.model';

@Injectable({
  providedIn: 'root'
})
export class InterventionService {

  private intervintion: Intervention;
  private readonly baseInterventionsUrl = "http://localhost:8080/api/v1";

  constructor(private httpClient: HttpClient) { }

  public getAll(id: number): Observable<Intervention[]> {
    return this.httpClient.get<Intervention[]>(`${this.baseInterventionsUrl}/cardboards/${id}/interventions`);
  }

  public save(intervention: Intervention): Observable<Intervention>{
    console.warn(intervention);
    return this.httpClient.post<Intervention>(`${this.baseInterventionsUrl}/cardboards/${intervention.cardboard.id}/interventions`, intervention);
  }
  public update(intervention: Intervention){
    return this.httpClient.put(`${this.baseInterventionsUrl}/cardboards/${intervention.cardboard.id}/interventions/${intervention.id}`,intervention);
  }
}
