import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { Cardboard } from 'src/app/cardboard/model/cardboard.model';
import { CardboarService } from 'src/app/cardboard/service/cardboar.service';
import { ServiceType } from 'src/app/service-type/model/serviceType.model';
import { ServiceTypeService } from 'src/app/service-type/service/service-type.service';
import { User } from 'src/app/user/model/user.model';
import { UserService } from 'src/app/user/service/user.service';
import { Intervention } from '../model/intervention.model';
import { Service } from '../model/service.model';
import { InterventionService } from '../service/intervention.service';


export interface InterventionComponentData {
  cardboard: Cardboard,
  intervention?: Intervention
}

@Component({
  selector: 'app-add-intervention',
  templateUrl: './add-intervention.component.html',
  styleUrls: ['./add-intervention.component.css']
})
export class AddInterventionComponent implements OnInit {

  cardboard: Cardboard;
  intervention?: Intervention;
  services: Service[] = [];

  interventionForm: FormGroup;
  serviceForm: FormGroup;
  dataSource = new MatTableDataSource<Service>();
  displayedColumns: string[] = ['description', 'serviceType', 'user'];

  serviceTypesSubscription: Subscription;
  serviceTypes: ServiceType[] = [];
  searchedServiceTypes: ServiceType[] = [];
  selectedServiceType: ServiceType;

  usersSubscription: Subscription;
  users: User[] = [];
  searchedUsers: User[] = [];
  selectedUser: User;

  constructor(private formBilder: FormBuilder,
              @Inject(MAT_DIALOG_DATA) public data: InterventionComponentData,
              private dialogRef: MatDialogRef<AddInterventionComponent>,
              private activatedRoute: ActivatedRoute,
              private cardboardService: CardboarService,
              private serviceTypeService: ServiceTypeService,
              private userService: UserService,
              private interventionService: InterventionService) { }

  ngOnInit(): void {

    this.cardboard = this.data.cardboard;
    if(this.data.intervention) {
      this.intervention = this.data.intervention;
      this.services = this.intervention.services;
      console.warn(this.services);
    }

    this.dataSource.data = this.services;
    this.initForm();
    this.fetchServiceTypes();
    this.fetchUsers();
    this.serviceType().valueChanges.subscribe(value => {
      this.searchedServiceTypes = this.serviceTypes.filter(serviceType => {
        return serviceType.name.toLowerCase().startsWith(value);
      }).slice();
    });
    this.user().valueChanges.subscribe(value => {
      this.searchedUsers = this.users.filter(user => {
        return (user.name.toLowerCase().startsWith(value) || user.lastname.toLowerCase().startsWith(value));
      }).slice();
    });
  }

  initForm(){
    let interventionDescription = this.intervention ? this.intervention.description : '';
    let createdDate = this.intervention ? this.intervention.createdDate : '';
    
    this.interventionForm = this.formBilder.group({
      description: [interventionDescription,[]],
      createdDate: [createdDate,[]]   
    });


    this.serviceForm = this.formBilder.group({
      description: ['',[]],
      serviceType: ['',[]],
      user: ['',[]]
    });
  }

  fetchServiceTypes(){
    this.serviceTypesSubscription = this.serviceTypeService.getAll().subscribe((types: ServiceType[]) => {
      this.serviceTypes = types.slice();
      this.searchedServiceTypes = types.slice();
    });
  }

  fetchUsers(){
    this.usersSubscription = this.userService.getAll().subscribe((users: User[]) => {
      this.users= users.slice();
      this.searchedUsers = users.slice();
    });
  }

  public serviceType(){
    return this.serviceForm.get('serviceType');
  }

  public description(){
    return this.interventionForm.get('description');
  }

  public createdDate(){
    return this.interventionForm.get('createdDate');
  }

  public user(){
    return this.serviceForm.get('user');
  }

  public descriptionService(){
    return this.serviceForm.get('description');
  }

  public addInterventionForm(data): Intervention {
    var intervention: Intervention = {
      id: 0,
      description: this.description().value,
      createdDate: this.createdDate().value,
      services: this.services,
      cardboard : this.cardboard
    }
    return intervention;
  }

  displayServiceType(type: ServiceType) {
    return type ? type.name : '';
  }

  displayUser(user: User) {
    return user ? user.name + user.lastname: '' ;
  }

  public addServiceForm(data){
    console.log(data);

    var service: Service = {
      id: 0,
      description: this.descriptionService().value,
      user: this.user().value,
      serviceType: this.serviceType().value
    }
    this.services.push(service);
    this.dataSource.data = this.services;
    console.log(this.services);
  }

  updateForm(){
    this.interventionForm = this.formBilder.group({
      description: ['',[]]
      //i ovde treba da azuriram tabelu sa uslugama
    })
  }
}
