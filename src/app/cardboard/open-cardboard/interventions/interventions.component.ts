import { AfterViewInit, ChangeDetectionStrategy, Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { Subscription } from 'rxjs';
import { Cardboard } from '../../model/cardboard.model';
import { AddInterventionComponent } from './add-intervention/add-intervention.component';
import { EditInterventionDialogComponent } from './edit-intervention-dialog/edit-intervention-dialog.component';
import { Intervention } from './model/intervention.model';
import { InterventionService } from './service/intervention.service';

@Component({
  selector: 'app-interventions',
  templateUrl: './interventions.component.html',
  styleUrls: ['./interventions.component.css']
})
export class InterventionsComponent implements OnInit, AfterViewInit {

  @Input('cardboard')
  cardboard: Cardboard;
  displayedColumns: string[] = ['Description', 'Created Date', 'Edit'];
  columNames: string[] = ['description', 'createdDate', 'edit'];
  dataSource : MatTableDataSource<Intervention>;
  interventionSubscription :Subscription;

  constructor(private interventionService: InterventionService,
              public dialog: MatDialog,
              private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource<Intervention>(this.cardboard.interventions);
    console.log(this.cardboard);
  }

  ngOnChanges() :void {
    this.dataSource = new MatTableDataSource<Intervention>(this.cardboard.interventions);
  }

  getIsoStirng(date: Date) : string {
    if(date) {
      return date.toISOString();
    }
    return '';
  }

  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  public editIntervention(intervention: Intervention){
    const dialogRef = this.dialog.open(AddInterventionComponent, {
      data: {
        cardboard: this.cardboard,
        intervention: intervention
      },
      width: '700px',
      height: '900px'
    });

    dialogRef.afterClosed().subscribe((result: Intervention) => {
      if(result) {
        result = {
          ...result,
          id: intervention.id
        }
        this.interventionService.update(result).subscribe((updatedIntervention: Intervention) => {
          this.snackBar.open('Intervention is successfully updated','', {
            duration: 5000
          });
          this.fetchInterventions();
        });
      }
    });
  }

  fetchInterventions() {
    this.interventionService.getAll(this.cardboard.id).subscribe((interventions: Intervention[]) => {
        this.cardboard.interventions = interventions.slice();
        this.dataSource = new MatTableDataSource<Intervention>(this.cardboard.interventions);
    });
  }
  
  public viewIntervention(intervention: Intervention){

  }

  getFormatedDate(date): string {
    if(date) {
      return new Date(date).toLocaleDateString();
    }
    return "";
  }
}
