import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { error } from 'protractor';
import { Observable, Subscription } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { nameValidation } from 'src/app/animal-type/add-animal-type/animal-type.validator';
import { AnimalType } from 'src/app/animal-type/model/animalType.model';
import { AnimalTypeService } from 'src/app/animal-type/service/animal-type.service';
import { Owner } from 'src/app/owner/model/owner.model';
import { OwnerService } from 'src/app/owner/service/owner.service';
import { threadId } from 'worker_threads';
import { Cardboard } from '../model/cardboard.model';
import { CardboarService } from '../service/cardboar.service';

@Component({
  selector: 'app-add-cardboard',
  templateUrl: './add-cardboard.component.html',
  styleUrls: ['./add-cardboard.component.css']
})
export class AddCardboarComponent implements OnInit, OnDestroy {

  cardboardForm: FormGroup;

  animalTypesSubscription: Subscription;
  animalTypes: AnimalType[] = [];
  searchedAnimalTypes: AnimalType[] = [];
  selectedAnimalType: AnimalType;

  ownerSubscription: Subscription;
  owners: Owner[] = [];
  searchedOwners: Owner[] = [];
  selectedOwner: Owner;

  constructor(private animalTypeService: AnimalTypeService,
                private ownerService: OwnerService,
                private formBilder: FormBuilder,
                private cardboardService: CardboarService,
                private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.initForm();
    this.fetchAnimalTypes();
    this.fetchOwners();
    this.animalType().valueChanges.subscribe(value => {
      this.searchedAnimalTypes = this.animalTypes.filter(animalType => {
        return animalType.name.startsWith(value);
      }).slice();
    });
    this.owner().valueChanges.subscribe(value => {
      this.searchedOwners = this.owners.filter(owner => {
        return (owner.name.startsWith(value) || owner.lastname.startsWith(value));
      }).slice();
    });

  }

  ngOnDestroy(): void {
    //this.animalTypesSubscription.unsubscribe();
  }

  fetchAnimalTypes(){
    this.animalTypesSubscription = this.animalTypeService.getAll().subscribe((types: AnimalType[]) => {
      this.animalTypes = types.slice();
      this.searchedAnimalTypes = types.slice();
    });
  }
  fetchOwners() {
    this.ownerSubscription = this.ownerService.getAll().subscribe((owners: Owner[]) => {
      this.owners = owners.slice();
      this.searchedOwners= owners.slice();
    });
  }

  public name() {
    return this.cardboardForm.get('name');
  }
  public birthday() {
    return this.cardboardForm.get('birthday');
  }
  public animalType() {
    return this.cardboardForm.get('animalType');
  }
  public owner() {
    return this.cardboardForm.get('owner');
  }

  initForm() {
    this.cardboardForm = this.formBilder.group({
      name: ['', [nameValidation()]],
      birthday: ['', []],
      animalType: ['', []],
      owner: ['', []]
    })
  }

  displayAnimalType(type: AnimalType) {
    return type ? type.name : '';
  }
  displayOwner(owner: Owner) {
    return owner ? (owner.name +" "+ owner.lastname) : '' ;
  }
  public addCardboardForm(data) {
    console.log(data);

    var cardboard: Cardboard = {
      id: 0,
      name: this.name().value,
      birthday: this.birthday().value,
      animalType: this.animalType().value,
      owner: this.owner().value,
      deactivated: false,
      createdDate: new Date,
      interventions : []
    };
    this.cardboardService.save(cardboard).subscribe(cardboard =>{
      this.snackBar.open('Cardboard is successfully saved','', {
        duration: 5000
      });
      this.initEmptyForm();
    }, error => {
      console.error(error);
      this.snackBar.open('Error on server side: Cardboard cannot be saved','', {
        duration: 5000
      });
    }); 
  }
  initEmptyForm(){
    this.cardboardForm = this.formBilder.group({
      name: ['',[nameValidation]],
      birthday: ['',[]],
      animalType: ['',[]],
      owner: ['',[]]
    });
  }
}
