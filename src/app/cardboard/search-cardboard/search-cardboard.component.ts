import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { error } from 'protractor';
import { Subscription } from 'rxjs';
import { EditCardboardDialogComponent } from '../edit-cardboard-dialog/edit-cardboard-dialog.component';
import { Cardboard } from '../model/cardboard.model';
import { CardboarService } from '../service/cardboar.service';

@Component({
  selector: 'app-search-cardboard',
  templateUrl: './search-cardboard.component.html',
  styleUrls: ['./search-cardboard.component.css']
})
export class SearchCardboardComponent implements OnInit, OnDestroy {


  cardboardSubscription: Subscription;
  cardboards: Cardboard[] = [];
  searchedCardboards: Cardboard[] = [];
  searchValue: string = '';

  constructor(private cardboradService: CardboarService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    private router: Router,
    private activatedRoute:ActivatedRoute) { }

  ngOnInit(): void {
    this.fetchCardboards();
  }

  ngOnDestroy(): void {
    this.cardboardSubscription.unsubscribe();
  }

  onSearchKeyup(event) {
    this.searchedCardboards = this.cardboards.filter(cardboard => {
      return cardboard.name.toLowerCase().startsWith(this.searchValue) ||
        cardboard.animalType.name.toLowerCase().startsWith(this.searchValue) ||
        cardboard.owner.name.toLowerCase().startsWith(this.searchValue) ||
        cardboard.owner.lastname.toLowerCase().startsWith(this.searchValue)
    }).slice();
  }

  fetchCardboards() {
    this.cardboardSubscription = this.cardboradService.getAll().subscribe((cardboards: Cardboard[]) => {
      this.cardboards = cardboards;
      this.searchedCardboards = cardboards;
    });
  }
  deactivatedCardboard(cardboard: Cardboard) {
    cardboard.deactivated = true;
    this.cardboradService.update(cardboard).subscribe(cardboard => {
      this.snackBar.open('Cardboard is successfully deactivated', '', {
        duration: 5000
      });
      this.fetchCardboards();

    }, error => {
      console.log(error);
      this.snackBar.open('Error on server side: Cardboard cannot be deactivated', '', {
        duration: 5000
      });
    });
  }

  editCardboard(cardboard) {
    const dialogRef = this.dialog.open(EditCardboardDialogComponent, {
      data: cardboard,
      width: '600px'
    });

    dialogRef.afterClosed().subscribe((result: Cardboard) => {
      if (result) {
        result = {
          ...result,
          id: cardboard.id
        }
        this.cardboradService.update(result).subscribe((updatedCardboard: Cardboard) => {
          this.snackBar.open('Cardboard is successfully updated', '', {
            duration: 5000
          });
          this.fetchCardboards();
        }, (error)=>{
          this.snackBar.open('Error on server side: Cardboard cannot be updated', '', {
            duration: 5000
          });
        });
      }
    });
  }

  openCardboard(cardboard: Cardboard) {
    this.router.navigate([`${cardboard.id}`], {relativeTo:this.activatedRoute});
  }
}
