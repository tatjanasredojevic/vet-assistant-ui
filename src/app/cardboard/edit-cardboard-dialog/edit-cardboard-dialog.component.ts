import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { AnimalType } from 'src/app/animal-type/model/animalType.model';
import { AnimalTypeService } from 'src/app/animal-type/service/animal-type.service';
import { Owner } from 'src/app/owner/model/owner.model';
import { OwnerService } from 'src/app/owner/service/owner.service';
import { nameValidation } from 'src/app/user/signup/signup.validator';
import { Cardboard } from '../model/cardboard.model';
import { CardboarService } from '../service/cardboar.service';

@Component({
  selector: 'app-edit-cardboard-dialog',
  templateUrl: './edit-cardboard-dialog.component.html',
  styleUrls: ['./edit-cardboard-dialog.component.css']
})
export class EditCardboardDialogComponent implements OnInit,OnDestroy {

  cardboardForm: FormGroup;

  animalTypesSubscription: Subscription;
  animalTypes: AnimalType[] = [];
  searchedAnimalTypes: AnimalType[] = [];
  selectedAnimalType: AnimalType;

  ownerSubscription: Subscription;
  owners: Owner[] = [];
  searchedOwners: Owner[] = [];
  selectedOwner: Owner;

  constructor(private animalTypeService: AnimalTypeService,
                private ownerService: OwnerService,
                private formBilder: FormBuilder,
                @Inject(MAT_DIALOG_DATA) private cardboard: Cardboard) { }

  ngOnInit(): void {
    this.initForm();
    this.fetchAnimalTypes();
    this.fetchOwners();
    this.animalType().valueChanges.subscribe(value => {
      this.searchedAnimalTypes = this.animalTypes.filter(animalType => {
        return animalType.name.startsWith(value);
      }).slice();
    });
    this.owner().valueChanges.subscribe(value => {
      this.searchedOwners = this.owners.filter(owner => {
        return (owner.name.startsWith(value) || owner.lastname.startsWith(value));
      }).slice();
    });
  }

  ngOnDestroy(){
    
  }
  fetchAnimalTypes(){
    this.animalTypesSubscription = this.animalTypeService.getAll().subscribe((types: AnimalType[]) => {
      this.animalTypes = types.slice();
      this.searchedAnimalTypes = types.slice();
    });
  }
  
  fetchOwners() {
    this.ownerSubscription = this.ownerService.getAll().subscribe((owners: Owner[]) => {
      this.owners = owners.slice();
      this.searchedOwners= owners.slice();
    });
  }

  initForm(){
    this.cardboardForm = this.formBilder.group({
      name: [this.cardboard.name,[nameValidation()]],
      birthday: [this.cardboard.birthday,[]],
      deactivated: false,
      createdDate: [this.cardboard.createdDate],
      animalType: [this.cardboard.animalType,[]],
      owner: [this.cardboard.owner,[]] 
    })
  }
  public name() {
    return this.cardboardForm.get('name');
  }
  public birthday() {
    return this.cardboardForm.get('birthday');
  }
  public animalType() {
    return this.cardboardForm.get('animalType');
  }
  public owner() {
    return this.cardboardForm.get('owner');
  }
  displayAnimalType(type: AnimalType) {
    return type ? type.name : '';
  }
  displayOwner(owner: Owner) {
    return owner ? (owner.name +" "+ owner.lastname) : '' ;
  }
}
