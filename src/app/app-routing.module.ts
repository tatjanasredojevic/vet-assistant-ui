import { from } from 'rxjs';
import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import { LoginComponent } from './user/login/login.component';
import { SignupComponent } from './user/signup/signup.component';
import { HomeComponent } from './home/home.component';
import { AddAnimalTypeComponent } from './animal-type/add-animal-type/add-animal-type.component';
import { SearchAnimalTypeComponent } from './animal-type/search-animal-type/search-animal-type.component';
import { AddServiceTypeComponent } from './service-type/add-service-type/add-service-type.component';
import { SearchServiceTypeComponent } from './service-type/search-service-type/search-service-type.component';
import { AddOwnerComponent } from './owner/add-owner/add-owner.component';
import { SearchOwnerComponent } from './owner/search-owner/search-owner.component';
import { AddCardboarComponent } from './cardboard/add-cardboard/add-cardboard.component';
import { SearchCardboardComponent } from './cardboard/search-cardboard/search-cardboard.component';
import { OpenCardboardComponent } from './cardboard/open-cardboard/open-cardboard.component';
import { AboutComponent } from './about/about/about.component';
import { AddInterventionComponent } from './cardboard/open-cardboard/interventions/add-intervention/add-intervention.component';
import { AuthGuardService } from './auth/auth-guard.service';
import { HomePageComponent } from './home-page/home-page.component';


const routes: Routes = [
    {path: 'login', component: LoginComponent},
    {path: 'signup', component: SignupComponent},
    {path: '', component: HomeComponent , canActivate:[AuthGuardService], children: [
        {path: '', component: HomePageComponent},
        {path: 'animalType/add', component: AddAnimalTypeComponent},
        {path: 'animalType/search', component: SearchAnimalTypeComponent},
        {path: 'serviceType/add', component: AddServiceTypeComponent},
        {path: 'serviceType/search', component: SearchServiceTypeComponent},
        {path: 'owner/add', component: AddOwnerComponent},
        {path: 'owner/search', component: SearchOwnerComponent},
        {path: 'cardboard/add', component: AddCardboarComponent},
        {path: 'cardboard/search', component: SearchCardboardComponent},
        {path: 'cardboard/search/:id', component: OpenCardboardComponent},
        {path: 'cardboard/search/:id/add-intervention', component: AddInterventionComponent},
        {path: 'about', component: AboutComponent}
    ]}
    
    // {path: 'cardboard/:cardboardId', component: CardboardComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule{}