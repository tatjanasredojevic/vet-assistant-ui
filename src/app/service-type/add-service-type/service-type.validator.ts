import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

export function nameValidation(): ValidatorFn {
    return (control : AbstractControl) : ValidationErrors | null =>{
        const nameIsCorrect = control.value.length > 1;
        return nameIsCorrect ? null :{incorrectName : true}
    }
}
