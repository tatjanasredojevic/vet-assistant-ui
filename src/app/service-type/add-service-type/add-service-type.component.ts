import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ServiceType } from '../model/serviceType.model';
import { ServiceTypeService } from '../service/service-type.service';
import { nameValidation } from './service-type.validator';

@Component({
  selector: 'app-add-service-type',
  templateUrl: './add-service-type.component.html',
  styleUrls: ['./add-service-type.component.css']
})
export class AddServiceTypeComponent implements OnInit {

  serviceTypeForm: FormGroup;

  constructor(private formBilder: FormBuilder,
              private serviceTypeService: ServiceTypeService,
              private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.initEmptyForm();
  }
  public name(){
    return this.serviceTypeForm.get('name');
  }

  public description(){
    return this.serviceTypeForm.get('description');
  }
  public marking(){
    return this.serviceTypeForm.get('marking');
  }

  initEmptyForm(){
    this.serviceTypeForm = this.formBilder.group({
      name: ['',[nameValidation()]],
      description: ['',[]],
      marking: ['',[]]
    })
  }

  public addServiceTypeForm(data){
    console.log(data);
    console.log(this.serviceTypeForm)

    var serviceType: ServiceType = {
      id: 0,
      name: this.name().value,
      description:  this.description().value,
      marking: this.marking().value
    };

    this.serviceTypeService.save(serviceType).subscribe(serviceType => {
      this.snackBar.open('Novi tip usluge je dodat!','', {
        duration: 5000
      });
      this.initEmptyForm();
    }, error=> {
      console.error(error);
    });
  }
}
