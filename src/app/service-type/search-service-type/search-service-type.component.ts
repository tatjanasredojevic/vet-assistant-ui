import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { config } from 'process';
import { error } from 'protractor';
import { Subscription } from 'rxjs';
import { types } from 'util';
import { AddServiceTypeComponent } from '../add-service-type/add-service-type.component';
import { EditServiceTypeDialogComponent } from '../edit-service-type-dialog/edit-service-type-dialog.component';
import { ServiceType } from '../model/serviceType.model';
import { ServiceTypeService } from '../service/service-type.service';

@Component({
  selector: 'app-search-service-type',
  templateUrl: './search-service-type.component.html',
  styleUrls: ['./search-service-type.component.css']
})
export class SearchServiceTypeComponent implements OnInit, OnDestroy {

  serviceTypes: ServiceType[] = [];
  searchedServiceTypes: ServiceType[] = [];
  searchedValue: string = '';
  serviceTypeSubsciption: Subscription;
  constructor(private serviceTypeService: ServiceTypeService,
              public dialog: MatDialog,
              private snackBar: MatSnackBar)
  { }

  ngOnInit(): void {
    this.fetchServiceType();
  }
  ngOnDestroy(){
    if(this.serviceTypeSubsciption) {
      this.serviceTypeSubsciption.unsubscribe;
    }
  }

  onSearchKeyup(event){
    this.searchedServiceTypes = this.serviceTypes.filter(type => {
      return type.name.toLowerCase().startsWith(this.searchedValue) || type.marking.toLowerCase().startsWith(this.searchedValue);
    }).slice();
  }

  public editServiceType(type: ServiceType) {
    
    const dialogRef = this.dialog.open(EditServiceTypeDialogComponent, {
      data: type,
      width: '600px'
    });

    dialogRef.afterClosed().subscribe((result: ServiceType) => {
      if(result) {
        result = {
          ...result,
          id: type.id
        }
        this.serviceTypeService.update(result).subscribe((updatedServiceType: ServiceType) => {
          this.snackBar.open('Tip usluge je uspešno izmenjen!','', {
            duration: 5000
          });
          this.fetchServiceType();
        });
      }
    });

  }

  public deleteServiceType(type: ServiceType){
    return this.serviceTypeService.delete(type.id).subscribe(type =>{
      this.snackBar.open('Uspesno obrisan tip usluge!','', {
        duration: 5000
      });
      this.fetchServiceType();
    },(error)=>{
      window.alert("Neuspesno brisanje tipa usluge");
    });
  }
  
  fetchServiceType(){
    this.serviceTypeService.getAll().subscribe((types: ServiceType[]) =>{
      this.serviceTypes = types;
      this.searchedServiceTypes = types
      });
  }
}
