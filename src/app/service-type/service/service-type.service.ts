import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ServiceType } from 'src/app/service-type/model/serviceType.model';

@Injectable({
  providedIn: 'root'
})
export class ServiceTypeService {

  constructor(private httpClient: HttpClient) { }

  private serviceType: ServiceType;

  private readonly baseServiceTypeUrl = "http://localhost:8080/api/v1/service-types";

  public save(serviceType: ServiceType) : Observable<ServiceType> {
    return this.httpClient.post<ServiceType>(this.baseServiceTypeUrl, serviceType);
  }
  public getAll(): Observable<ServiceType[]>{
    return this.httpClient.get<ServiceType[]>(this.baseServiceTypeUrl);
  }

  public delete(id: number){
    return this.httpClient.delete(`${this.baseServiceTypeUrl}/${id}`);
  }

  public update(serviceType: ServiceType){
    return this.httpClient.put(`${this.baseServiceTypeUrl}/${serviceType.id}`, serviceType);
  }
}
