import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { nameValidation } from 'src/app/user/signup/signup.validator';
import { ServiceType } from '../model/serviceType.model';

@Component({
  selector: 'app-edit-service-type-dialog',
  templateUrl: './edit-service-type-dialog.component.html',
  styleUrls: ['./edit-service-type-dialog.component.css']
})
export class EditServiceTypeDialogComponent implements OnInit {

  serviceTypeForm: FormGroup;

  constructor(private formBilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public serviceType: ServiceType) { }

  ngOnInit(): void {
    this.initForm();
  }

  initForm(){
    this.serviceTypeForm = this.formBilder.group({
      name: [this.serviceType.name,[nameValidation()]],
      description: [this.serviceType.description,[]],
      marking: [this.serviceType.marking,[]]
    })
  }

  editServiceTypeForm(formValue) {

  }

}
